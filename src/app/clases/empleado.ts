export class Empleado {
    
    id:number;
    primerApellido:String;
    segundoApellido:String;
    primerNombre:String;
    otroNombre:String;
    paisEmpleo:String;
    tipoIdentificacion:String;
    nroIdentificacion:String;
    correoElectronico:String;
    fechaIngreso:Date;
    area:String;
    estado:String;
    fechaHoraRegistro:Date;
}
