import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {DataTablesModule} from 'angular-datatables'; 

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddEmpleadoComponent } from './components/add-empleado/add-empleado.component';
import { EmpleadoDetailsComponent } from './components/empleado-details/empleado-details.component';
import { EmpleadosListComponent } from './components/empleados-list/empleados-list.component';

@NgModule({
  declarations: [
    AppComponent,
    AddEmpleadoComponent,
    EmpleadoDetailsComponent,
    EmpleadosListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,  
    HttpClientModule,  
    DataTablesModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
